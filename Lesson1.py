products = [
    ('Макароны', 354),
    ('Картофель', 435),
    ('Помидоры', 200),
    ('Минеральная вода', 50)
]
check_width = 30
horizontal_border = '_' * check_width
currency = ' руб.|'
check = ''
for product_name, product_price in products:
    spaces_amount = check_width - 8 - len(str(product_price))-len(str(product_name))
    check += '|%s' % product_name + ' ' * spaces_amount  + ' %s' % product_price + currency + '\n'
check = horizontal_border + '\n'+ '|' +' '*(check_width - 2) + '|' + '\n' + check  + '|' +'_'*(check_width - 2) + '|'
print(check)
